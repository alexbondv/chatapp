﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatApp.DataAccess;
using ChatApp.Service.ViewModels;

namespace ChatApp.Service.Mappers
{
    public class MessageMapping
    {
        public Message MapMessageToEntity(SendMessageViewModel view)
        {
            var message = new Message();
            message.Subject = view.Subject;
            message.Body = view.Body;
            message.IsSent = view.IsSent;

            var recipients = view.RecipientsIds.Select(idUser => new User { UserId = Guid.Parse(idUser) }).ToList();

            message.Recipients = recipients;

            return message;
        }
    }
}
