﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Service.ViewModels
{
    public class SendMessageViewModel
    {
        public Guid MessageId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsSent { get; set; }
        public List<string> RecipientsIds { get; set; }
   
    }
}
