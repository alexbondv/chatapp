﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatApp.DataAccess;
using ChatApp.DataAccess.Repositories;
using ChatApp.Service.Mappers;
using ChatApp.Service.ViewModels;

namespace ChatApp.Service
{
    public class MessageService
    {
        private EntityContext _entityContext;
        private MessageMapping _messageMapping;

        private readonly MessageRepository _messageRepository;
        private IDbConnection _dbConnection;

        public MessageService(string connection)
        {
            _dbConnection = new SqlConnection(connection);
            _entityContext = new EntityContext(connection);
            _messageRepository = new MessageRepository(_entityContext);
            _messageMapping = new MessageMapping();
        }

        public bool SendMessage(SendMessageViewModel view)
        {
            try
            {
                Message message = _messageMapping.MapMessageToEntity(view);
                _messageRepository.AddMessage(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Message> GetUserMessages(string userId)
        {
            try
            {
                List<Message> userMessages = _messageRepository.GetUserMessages(Guid.Parse(userId));
                return userMessages;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
