﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.DataAccess
{
    class DbInitializer : System.Data.Entity.CreateDatabaseIfNotExists<EntityContext>
    {
        protected override void Seed(EntityContext context)
        {
            var users = new List<User>
            {
            new User{Name = "Mike"},
            new User{Name = "Ann"},
            };

            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();          
        }
    }
}
