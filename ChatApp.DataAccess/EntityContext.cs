﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace ChatApp.DataAccess
{
    public class EntityContext : DbContext
    {

        public EntityContext()
            : base("DbConnection")
        { }

        public EntityContext(string connectionString)
            : base(connectionString)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }

    }
}