﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.DataAccess.Repositories
{
    public class MessageRepository
    {
        private EntityContext _entityContext;

        public MessageRepository(EntityContext context)
        {
            _entityContext = context;
        }

        public void AddMessage(Message message)
        {
            _entityContext.Messages.Add(message);
            _entityContext.SaveChanges();
        }

        public List<Message> GetUserMessages(Guid userId)
        {
            var userMessages = _entityContext.Messages.Select(mes => mes)
                .Where(x => x.Recipients
                    .Any(user => user.UserId == userId))
                    .ToList();

            return userMessages;
        }
    }
}
