﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.DataAccess
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public User()
        {
            UserId = Guid.NewGuid();
        }
    }
}
