﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.DataAccess
{
    public class Message
    {
        public Guid MessageId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsSent { get; set; }

        public virtual ICollection<User> Recipients { get; set; }

        public Message()
        {
            MessageId = Guid.NewGuid();
        }
    }
}
