﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatApp.Web
{
    public interface IHubClient
    {
        Task SendNotification(string messageId, bool isSent);
    }
}
