﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using ChatApp.Service;
using ChatApp.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.SignalR;
using ChatApp.Web;

namespace ChatApp.Controllers
{
    [Route("api/[controller]")]

    public class MessageController : Controller
    {

        private readonly MessageService _service;
        private readonly IConfiguration configuration;
        private IHubContext<NotificationHub, IHubClient> _hubContext;

        public MessageController(IConfiguration config, IHubContext<NotificationHub, IHubClient> hubContext)
        {
            configuration = config;
            _hubContext = hubContext;
            string connectionString = configuration.GetConnectionString("DbConnection");

            _service = new MessageService(connectionString);
        }

        [HttpPost]
        public bool Send([FromBody]SendMessageViewModel message)
        {
            var isSent = _service.SendMessage(message);

            SendNotification(message.MessageId.ToString(), isSent);
            return isSent;
        }

        private void SendNotification(string messageId, bool isSent)
        {
            _hubContext.Clients.All.SendNotification(messageId, isSent);
        }
    }
}